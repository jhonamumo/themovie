from django import forms

"""
Search Form is the class that containing the attributes to be
displayed in the template.
"""
class SearchForm(forms.Form):
    OPTIONS_LIST = (
        ('M', 'MOVIE'),
        ('T', 'TV SHOW'),
        #('P', 'PERSON'),
    )

    pattern_form = forms.CharField(label='Pattern', max_length=140)
    option_form = forms.ChoiceField(label='Option',
                                    widget=forms.Select(),
                                    choices=OPTIONS_LIST)
