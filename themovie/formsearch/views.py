from connection import API_KEY
from django.shortcuts import render
from forms import *

import tmdbsimple as tmdb
tmdb.API_KEY = API_KEY


def search_form(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            result = do_search(request)
            return render(request,
                          'search_list.html',
                          {'form': form, 'results': result})
    else:
        form = SearchForm()

    return render(request, 'search_list.html', {'form': form})


def do_search(request):
    pattern = request.POST['pattern_form']
    search = tmdb.Search()
    if request.POST['option_form'] == 'M':
        search.movie(query=pattern)
        date_info = 'release_date'
    elif request.POST['option_form'] == 'T':
        search.tv(query=pattern)
        date_info = 'first_air_date'
    elif request.POST['option_form'] == 'P':
        search.person(query=pattern)
        date_info = 'id'
    results = sorted(search.results, key=lambda k: k[date_info])
    data_info = info_result(results)
    return data_info


def info_result(data):
    data_info = []
    for l in data:
        id_cliente = l.get('id', None)
        try:
            data_info.append(tmdb.Movies(id_cliente).info())
        except:
            print "no existe {0}".format(id_cliente)
            data_info.append('Nada')

    return data_info
