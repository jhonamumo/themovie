from celery import task
from connection import API_KEY
import tmdbsimple as tmdb
tmdb.API_KEY = API_KEY

"""
"""
@task()
def info_result(data):
    data_info= []
    for l in data:
        id_cliente = l.get('id', None)
        try:
            data_info.append(tmdb.Movies(id_cliente).info())
        except:
            print "no existe {0}".format(id_cliente)
            data_info.append('Nada')

    return data_info
