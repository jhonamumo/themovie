from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'themovie.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'formsearch.views.search_form', name='search_form'),
    url(r'^search/$', 'formsearch.views.search_form', name='search_list'),
)
